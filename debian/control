Source: vsearch
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               architecture-is-64-bit,
               architecture-is-little-endian,
               zlib1g-dev,
               libbz2-dev,
               libtext-markdown-perl,
               ghostscript,
               libsimde-dev [!amd64 !arm64 !ppc64el],
               time
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/vsearch
Vcs-Git: https://salsa.debian.org/med-team/vsearch.git
Homepage: https://github.com/torognes/vsearch/
Rules-Requires-Root: no

Package: vsearch
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Built-Using: ${simde:Built-Using}
Description: tool for processing metagenomic sequences
 Versatile 64-bit multithreaded tool for processing metagenomic sequences,
 including searching, clustering, chimera detection, dereplication, sorting,
 masking and shuffling
 .
 The aim of this project is to create an alternative to the USEARCH tool
 developed by Robert C. Edgar (2010). The new tool should:
 .
  - have a 64-bit design that handles very large databases and much more
    than 4GB of memory
  - be as accurate or more accurate than usearch
  - be as fast or faster than usearch

Package: vsearch-examples
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends}
Enhances: vsearch
Description: Test Data for vsearch tool for processing metagenomic sequences
 Versatile 64-bit multithreaded tool for processing metagenomic sequences,
 including searching, clustering, chimera detection, dereplication, sorting,
 masking and shuffling
 .
 The aim of this project is to create an alternative to the USEARCH tool
 developed by Robert C. Edgar (2010). The new tool should:
 .
  - have a 64-bit design that handles very large databases and much more
    than 4GB of memory
  - be as accurate or more accurate than usearch
  - be as fast or faster than usearch
 .
 This package contains a test data set as well as sample scripts
 running some test suite provided by Debian also as autopkgtest.
